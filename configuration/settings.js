//You can either put your hex in your embeds module, or here, your choice.
const settings = {
    "prefix" : ">",
    "owners" : ["200654056992145408"],
    "token" : process.env.TOKEN,
    "modRole" : "AxelController",
    "muteRole" : "Axel-Mute",
    "agreeRole" : "NotAgreed",
    "muteChannelName": "mod-logs",
    "hex": "0xff0000",
    "youtubeAPIKey": process.env.API,
    "overflow" : 3155760000000000,
    "weekOverflow": 6.048e+8,
};

module.exports = settings;
