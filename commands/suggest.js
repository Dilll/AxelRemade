exports.run = function(bot, msg, args) {
    const suggestion = args.join(" ");
    let user = bot.fetchUser('200654056992145408')
    .then(user => {
        user.send(`__**Suggestion:**__\n**From:** ${msg.author.tag} <${msg.author.id}>\n**Description:** ${suggestion}`).then(() => {
            bot.embed(msg, bot.hex, "Successfully sent suggestion!", 'Thanks for the suggestion! It has been sent to Dylan for him to review! It has also been logged in the #suggestions channel for others to vote 👍 or 👎!')
            msg.guild.channels.find("name", "suggestions").send(`__**Suggestion:**__\n**From:** ${msg.author.tag} <${msg.author.id}>\n**Description:** ${suggestion}`)
            .then((newMessage) => {
                setTimeout(function(){ 
                newMessage.react("👍")
                }, 1000)
                setTimeout(function(){ 
                newMessage.react("👎")
                }, 1500)
            })
        })
    });
    };
    
    exports.conf = {
        activated: true,
        aliases: ['suggestion'],
        permLevel: 0
    };
        
      exports.help = {
        name: 'suggest',
        description: 'Sends your suggestion to my owner!',
        usage: 'suggest <thing>, suggestion <thing>'
    };